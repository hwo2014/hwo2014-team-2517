import json
import socket
import sys
import os
from collections import namedtuple

Message = namedtuple('Message', 'type data id tick')
DEBUG = os.getenv("DEBUG", False)
if DEBUG:
    log = open(os.path.join("env", "debug.log"), "w")


class Client(object):
    __tick = None

    def __init__(self, file):
        self.file = file

    def send(self, str):
        if DEBUG:
            log.write(str + '\n')
        self.file.write(str + "\n")
        self.file.flush()

    def msg(self, msg_type, **args):
        self.send(json.dumps(dict({'msgType': msg_type}, **args)))

    def join(self, name, key):
        self.msg("join", data={'name': name, 'key': key})

    def ping(self):
        self.msg("ping")

    def throttle(self, throttle):
        self.msg("throttle", data=throttle, gameTick=self.__tick)

    def start(self, name, key):
        def check_msg(msg, **args):
            if not msg:
                return None;
            for key, value in args.items():
                if value != msg[key]:
                    raise RuntimeError("Required {0}:{1}. Got {2}"
                                       .format(key, value, msg[key]))
            return msg

        def get_msg():
            str = self.file.readline()
            if not str:
                return None
            if DEBUG:
                log.write(str.rstrip("\n"))
            msg = json.loads(str)
            return Message(msg['msgType'], msg['data'],
                           msg.get('gameId'), msg.get('gameTick'))

        self.join(name, key)
        if not check_msg(get_msg(), type="join"): return
        self.ping() 
        msg = check_msg(get_msg(), type="yourCar")
        if not msg: return
        id = msg.id
        self.ping() 
        if not check_msg(get_msg(), type="gameInit", id=id): return
        self.ping()
        while True:
            msg = check_msg(get_msg(), id=id) 
            if not msg: return
            type, _, _, self.__tick = msg
            if type == "carPositions":
                self.throttle(0.5)
            else:
                print("Got {0}".format(type))
                self.ping()

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        client = Client(s.makefile())
        client.start(name, key)
