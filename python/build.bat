@pushd %~dp0
@virtualenv env --no-site-packages --distribute
@call env/Scripts/activate.bat
@popd
