@set THIS = %0
@pushd %~dp0
@call config.bat
@call scripts/check_main_args.bat %*
@if errorlevel 1 @goto :Exit
@if not [%3] == [] @set BOTNAME="%3"

@pushd "%PL%"
@call run.bat %1 %2 %BOTNAME% %BOTKEY%
@popd

:Exit
@popd
