@set THIS = %0
@pushd %~dp0
@call config.bat
@pushd "%PL%"
@call build.bat %*
@popd
@popd
